;; Define package repositories
(require 'package)

(add-to-list 'package-archives
             '("marmalade" . "https://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("gnu" . "https://elpa.gnu.org/packages/") t)

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

(defun get-full-path (@file-relative-path)
  (concat (file-name-directory (or load-file-name buffer-file-name))
          @file-relative-path))

;;(add-to-list 'load-path "~/emacs/externs/")

;; TEMP FIX
(load (get-full-path "local-externs/lv"))
(load (get-full-path "local-externs/prettier-js"))
;; 

(load (get-full-path "settings"))
(load (get-full-path "externs/packages"))


;; (load (get-full-path "fonts/pretty-fonts"))

;; (use-package pretty-fonts
;;   :init
;;   (pretty-fonts-set-fontsets-for-fira-code)
;;   :config 
;;   (pretty-fonts-add-hook prog-mode-hook pretty-fonts-fira-code-alist)
;;   (pretty-fonts-add-hook org-mode-hook pretty-fonts-fira-code-alist))
