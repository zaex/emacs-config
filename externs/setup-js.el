(use-package js2-mode
  :ensure t
  :mode "\\.js\\'"
  :interpreter "node"
  :init
  (setq-default js2-concat-multiline-strings 'eol)
  (setq-default js2-global-externs '("module" "require" "setTimeout" "clearTimeout" "setInterval"
                                     "clearInterval" "location" "__dirname" "console" "JSON" "window"
                                     "process" "fetch"))
  (setq-default js2-strict-trailing-comma-warning t)
  (setq-default js2-strict-inconsistent-return-warning nil)
  :bind (:map
         js2-mode-map
         ("M-r"        . node-js-eval-region-or-buffer)
         ("M-R"        . refresh-chrome)
         ("M-s-<up>"   . js2r-move-line-up)
         ("M-s-<down>" . js2r-move-line-down)
         ("C-<left>"   . js2r-forward-barf)
         ("C-<right>"  . js2r-forward-slurp)
         ("M-m S"      . js2r-split-string))
  :config
  (use-package prettier-js
    :config
    (add-hook 'js2-mode-hook 'prettier-js-mode)
    (add-hook 'web-mode-hook 'prettier-js-mode))
  (use-package rjsx-mode :ensure t
    :mode "\\.jsx\\'"
    :magic ("import React" . rjsx-mode))
  (use-package js2-refactor :ensure t)
  (use-package json-mode :ensure t)
  (use-package nodejs-repl :ensure t)
  (add-hook 'js2-mode-hook #'js2-refactor-mode))

(provide 'setup-js)
;;; setup-js.el ends here
