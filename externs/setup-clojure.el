(use-package paredit :ensure t)
(use-package smart-tab :ensure t)
(use-package smart-yank :ensure t)
(use-package flycheck-clj-kondo :ensure t)

(use-package parinfer
  :ensure t
  :after (paredit)
  :bind
  (("C-<left>" . paredit-forward-barf-sexp)
   ("C-<right>" . paredit-forward-slurp-sexp)
   ("C-(" . paredit-forward-barf-sexp)
   ("C-)" . paredit-forward-slurp-sexp)
   ("C-S-<right>" . paredit-backward-barf-sexp)
   ("C-S-<left>" . paredit-backward-slurp-sexp)
   ("C-}" . paredit-backward-barf-sexp)
   ("C-{" . paredit-backward-slurp-sexp)
   :map
   parinfer-mode-map
   ("<tab>" . parinfer-smart-tab:dwim-right)
   ("S-<tab>" . parinfer-smart-tab:dwim-left)
   ("C-," . parinfer-toggle-mode)
   :map
   parinfer-region-mode-map
   ("C-," . parinfer-toggle-mode)
   ("<tab>" . parinfer-smart-tab:dwim-right)
   ("S-<tab>" . parinfer-smart-tab:dwim-left))
  :hook
  ((emacs-lisp-mode
    common-lisp-mode
    scheme-mode
    lisp-mode
    lisp-interaction-mode) . parinfer-mode)
  :custom
  (parinfer-lighters '(" ->" . " ()"))
  (parinfer-extensions '(defaults pretty-parens paredit smart-tab smart-yank)))


(use-package clojure-mode
  :ensure t
  :after (parinfer flycheck-clj-kondo)
  :mode (("\\.edn$" . clojure-mode)
         ("\\.boot$" . clojure-mode)
         ("\\.cljs.*$" . clojurescript-mode)
         ("^lein-env$" . clojure-mode))
  :hook ((clojure-mode . parinfer-mode))
  :config
  (use-package clojure-mode-extra-font-locking :ensure t))
  ;(add-hook 'clojure-mode-hook 'parinfer-mode))


(use-package cider
  :ensure t
  :pin melpa-stable
  :after (clojure-mode)
  :init
  (add-hook 'cider-mode-hook #'eldoc-mode)
  (add-hook 'cider-repl-mode-hook #'parinfer-mode)
  :custom
  (cider-repl-pop-to-buffer-on-connect t)
  (cider-show-error-buffer t)
  (cider-auto-select-error-buffer t)
  (cider-repl-history-file "~/.emacs.d/cider-history"))

(use-package clj-refactor
  :ensure t)

(defun my-clojure-mode-hook ()
    (clj-refactor-mode 1)
    (yas-minor-mode 1) ; for adding require/use/import statements
    ;; This choice of keybinding leaves cider-macroexpand-1 unbound
    (cljr-add-keybindings-with-prefix "C-c C-m"))

(add-hook 'clojure-mode-hook #'my-clojure-mode-hook)

(provide 'setup-clojure)
;;; setup-clojure.el ends here
