(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; A way to require system packages
(use-package use-package-ensure-system-package
  :ensure t)

(use-package projectile
  :ensure t
  :pin melpa-stable
  :config
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))

;; (use-package auto-package-update
;;   :ensure t
;;   :config
;;   (setq auto-package-update-delete-old-versions t)
;;   (setq auto-package-update-hide-results t)
;;   (auto-package-update-maybe))

(use-package ido
  :ensure t
  :bind (("C-x C-b" . ibuffer))
  :custom
  (ido-enable-flex-matching t "This allows partial matches")
  (ido-use-filename-at-point nil "Disable, it's annoying")
  (ido-auto-merge-work-directories-length -1 "Only match in current directory")
  (ido-use-virtual-buffers t "Recently open files")
  :config
  (add-to-list 'ido-ignore-files "\\.DS_Store")
  (ido-mode 1)
  (ido-everywhere 1))

;; (use-package ido-vertical-mode
;;   :ensure t
;;   :after (ido)
;;   :config (ido-vertical-mode 1))

(use-package flx-ido
  :ensure t
  :requires (ido)
  :custom (ido-use-faces nil)
  :config (flx-ido-mode 1))

(use-package ido-completing-read+
  :ensure t
  :requires (ido)
  :config (ido-ubiquitous-mode 1))

(use-package ido-yes-or-no
  :ensure t
  :requires (ido)
  :config (ido-yes-or-no-mode 1))

(use-package smex
  :ensure t
  :after (ido)
  :bind
  ("M-x" . smex)
  ("M-X" . smex-major-mode-commands)
  ("C-c C-c M-x" . execute-extended-command)
  :config
  (setq smex-save-file (concat user-emacs-directory ".smex-items"))
  (smex-initialize))

(use-package uniquify
  ;; is not a 'package.el' package, hence nil
  :ensure nil
  :custom
  (uniquify-buffer-name-style 'forward)
  (uniquify-separator "/")
  (uniquify-after-kill-buffer-p t)
  (uniquify-ignore-buffers-re "^\\*"))

(use-package recentf
  :ensure t
  :custom 
  (recentf-max-menu-items 40)
  :config 
  (setq recentf-save-file (concat user-emacs-directory ".recentf"))
  (recentf-mode 1))

(use-package company
  :ensure t
  :hook ((after-init . global-company-mode)))

(use-package rainbow-delimiters :ensure t)

(use-package exec-path-from-shell
  :if (memq window-system '(mac ns))
  :ensure t
  :config (exec-path-from-shell-initialize)) 


;;(use-package command-log-mode :ensure t)

;; (use-package projectile
;;   :config (projectile-global-mode))


(use-package yaml-mode
  :ensure t
  :mode (("\\.yml$" . yaml-mode)
         ("\\.yaml$" . yaml-mode)))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package flycheck-yamllint
  :ensure t
  :defer t
  :init
  (progn
    (eval-after-load 'flycheck
      '(add-hook 'flycheck-mode-hook 'flycheck-yamllint-setup))))

(use-package groovy-mode
  :ensure t
  :mode (("\\.gradle$" . groovy-mode)
         ("^Jenkins$" . groovy-mode)))

(use-package dockerfile-mode
  :ensure t
  :mode (("^DockerFile$" . dockerfile-mode))
  :config (put 'dockerfile-image-name 'safe-local-variable #'stringp))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))


;; (use-package pretty-mode
;;   :ensure t
;;   :config
;;   ;;(set-frame-font "Fira Code")
;;   (global-pretty-mode t)
;;   (global-prettify-symbols-mode 1)
;;   (pretty-fonts-set-kwds
;;     '((pretty-fonts-fira-font prog-mode-hook org-mode-hook)))
;;   (pretty-deactivate-groups
;;     '(:equality :ordering :ordering-double :ordering-triple
;;                 :arrows :arrows-twoheaded :punctuation
;;                 :logic :sets))
;;   (pretty-activate-groups
;;    '(:sub-and-superscripts :greek :arithmetic-nary)))



;; (require 'pretty-mode)
;; (global-pretty-mode t)

;; (pretty-deactivate-groups
;;  '(:equality :ordering :ordering-double :ordering-triple
;;              :arrows :arrows-twoheaded :punctuation
;;              :logic :sets))

;; (pretty-activate-groups
;;  '(:sub-and-superscripts :greek :arithmetic-nary))

;; load the rest


(load (get-full-path "setup-clojure"))
(load (get-full-path "setup-js"))
(load (get-full-path "setup-rust"))

(provide 'packages)
;;; packages.el ends here

